﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gestor_de_Agenda.Models;

namespace Gestor_de_Agenda.Controllers
{
    class ControladorAgenda
    {
        private Agenda agenda;

        public Agenda carregarAgenda(DonoAgenda usuario)
        {
            return usuario.solicitaAgenda(usuario);
        }
    }
}
