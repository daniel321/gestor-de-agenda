﻿using Gestor_de_Agenda.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestor_de_Agenda
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            TelaLogin TelaLogin = new TelaLogin();
            TelaLogin.ShowDialog();

            if (TelaLogin.getLogado())
            {
                TelaPrincipal TelaPrincipal = new TelaPrincipal();
                Application.Run(TelaPrincipal);
                TelaPrincipal.setUsuario(TelaLogin.getUsuario());
            }
        }
    }
}
