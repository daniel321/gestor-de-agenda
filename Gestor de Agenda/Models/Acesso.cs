﻿using System;
using System.Collections;

namespace Gestor_de_Agenda.Models
{
    class AutorizacoesAcesso
    {
        private ArrayList listaTerceiros;
        private ArrayList listaAutorizacoes;

        public void autorizar(Terceiro terceiro)
        {
            listaTerceiros.Add(terceiro);
        }
        public void desautorizar(Terceiro terceiro)
        {
            listaTerceiros.Remove(terceiro);
        }
        public void adicionarAutorizacao(Permissao permissao)
        {
            listaAutorizacoes.Add(permissao);
        }
        public void removerAutorizacao(Permissao permissao)
        {
            listaAutorizacoes.Remove(permissao);
        }
        public ArrayList getTerceiros(){
            return listaTerceiros;
        }
        public ArrayList getAutorizacoes()
        {
            return listaAutorizacoes;
        }
    }
}
