﻿using System;
using System.Collections;

namespace Gestor_de_Agenda.Models
{
    class Agenda
    {
        private ArrayList listaTarefas;
        private ArrayList listaCompromissos;
        private ArrayList listaLembretes;
        public AutorizacoesAcesso autorizacoes;

        public void adicionarAgendamento(Tarefa tarefa)
        {
            listaTarefas.Add(tarefa);
        }
        public void adicionarAgendamento(Compromisso compromisso)
        {
            listaCompromissos.Add(compromisso);
        }
        public void adicionarAgendamento(Lembrete lembrete)
        {
            listaLembretes.Add(lembrete);
        }
    }
}
