﻿using System;
using System.Collections;

namespace Gestor_de_Agenda.Models
{
    class Agendamento
    {
        public Agendamento()
        { }
        public Agendamento(String titulo, Tempo tempoAtual)
        {
            this.titulo = titulo;
            tempoCriacao = tempoAtual;
        }
        public String titulo;
        private Tempo tempoCriacao;
        public AutorizacoesAcesso autorizacoes;
        public Tempo getTempoCriacao()
        {
            return tempoCriacao;
        }
    }

    class Lembrete : Agendamento
    {
        public Tempo tempoAtivacao;
        public String mensagem;
    }

    class Compromisso : Agendamento
    {
        public ArrayList ListaDias;
        public Tempo horaDuracao;
        public String discrissao;
    }

    class Tarefa : Agendamento
    {
        public ArrayList ListaDias;
        public Tempo horaDuracao;
    }
}
