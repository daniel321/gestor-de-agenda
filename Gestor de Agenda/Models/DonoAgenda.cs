﻿using System;
using System.Collections;

namespace Gestor_de_Agenda.Models
{
    class DonoAgenda : Usuario
    {
        private Agenda agenda;

        public Agenda solicitaAgenda(Terceiro solicitante)
        {
            foreach (Terceiro terceiro in agenda.autorizacoes.getTerceiros())
            {
                if (solicitante.Equals(terceiro))
                    return agenda;
            }
            foreach (Permissao permissao in solicitante.getPermissoes())
            {
                foreach (Permissao autorizacao in agenda.autorizacoes.getAutorizacoes())
                {
                    if (permissao.Equals(autorizacao))
                        return agenda;
                }
            }
            return null;
        }
        public Agenda solicitaAgenda(DonoAgenda solicitante)
        {
            if (this.Equals(solicitante))
                return agenda;
            else return null;
        }
    }
}
