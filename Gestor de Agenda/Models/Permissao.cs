﻿using System;

namespace Gestor_de_Agenda.Models
{
    class Permissao
    {
        public Permissao(String nome, String chave)
        {
            this.nome = nome;
            this.chave = chave;
        }
        public String nome;
        private String chave;

        public String getChave()
        {
            return chave;
        }

    }
}
