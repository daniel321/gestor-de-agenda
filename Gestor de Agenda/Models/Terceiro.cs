﻿using System;
using System.Collections;

namespace Gestor_de_Agenda.Models
{
    class Terceiro : Usuario
    {
        private ArrayList permissoesAcesso;//<Permissao>
        private ArrayList listaDonosAgendas;//<DonoAgenda>

        public bool adicionaAgenda(DonoAgenda donoAgenda, ArrayList permissao)
        {
            Agenda agenda = donoAgenda.solicitaAgenda(this);
            if (agenda.Equals(null))
                return false;
            listaDonosAgendas.Add(donoAgenda);
            return true;
        }
        public void adicionaPermissao(Permissao permissao)
        {
            permissoesAcesso.Add(permissao);
        }
        public ArrayList getPermissoes()
        {
            return permissoesAcesso;
        }
        public Agenda solicitaAgenda(DonoAgenda donoEspecifico)
        {
            foreach (DonoAgenda donoAgenda in listaDonosAgendas)
            {
                if (donoAgenda.Equals(donoEspecifico))
                {
                    return donoEspecifico.solicitaAgenda(this);
                }
            }
            return null;
        }
    }
}
