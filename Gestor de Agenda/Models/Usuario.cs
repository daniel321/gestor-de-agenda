﻿using System;
using System.Collections;

namespace Gestor_de_Agenda.Models
{
    class Usuario
    {
        public Usuario()
        {

        }
        public Usuario(String username, String password)
        {
            this.username = username;
            this.password = password;
        }
        public Usuario(String username, String password, String nome, String sobrenome, String dataNascimento)
        {
            this.username = username;
            this.password = password;
            this.nome = nome;
            this.sobrenome = sobrenome;
            this.dataNascimento = dataNascimento;
        }

        private String username;
        private String password;
        public String nome;
        public String sobrenome;
        private String dataNascimento;

        public bool mudarPassword(String senhaAntiga, String senhaNova)
        {
            if (senhaAntiga == password)
            {
                password = senhaNova;
                return true;
            }
            else
                return false;
        }
        public Agenda solicitaAgenda(Usuario solicitante);
    }
}
