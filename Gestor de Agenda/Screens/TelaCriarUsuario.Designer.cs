﻿namespace Gestor_de_Agenda.Screens
{
    partial class TelaCriarUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.campoNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.campoSobrenome = new System.Windows.Forms.TextBox();
            this.campoUsername = new System.Windows.Forms.TextBox();
            this.campoPassword = new System.Windows.Forms.TextBox();
            this.campoPassword2 = new System.Windows.Forms.TextBox();
            this.campoDataNascimento = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.radioDonoAgenda = new System.Windows.Forms.RadioButton();
            this.radioTerceiro = new System.Windows.Forms.RadioButton();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(268, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Preencha os campos abaixo para criar uma nova conta";
            // 
            // campoNome
            // 
            this.campoNome.Location = new System.Drawing.Point(133, 80);
            this.campoNome.Name = "campoNome";
            this.campoNome.Size = new System.Drawing.Size(217, 20);
            this.campoNome.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nome:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Sobrenome:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Username:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 228);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Senha:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 261);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Confirmar senha:";
            // 
            // campoSobrenome
            // 
            this.campoSobrenome.Location = new System.Drawing.Point(133, 117);
            this.campoSobrenome.Name = "campoSobrenome";
            this.campoSobrenome.Size = new System.Drawing.Size(217, 20);
            this.campoSobrenome.TabIndex = 7;
            // 
            // campoUsername
            // 
            this.campoUsername.Location = new System.Drawing.Point(133, 190);
            this.campoUsername.Name = "campoUsername";
            this.campoUsername.Size = new System.Drawing.Size(217, 20);
            this.campoUsername.TabIndex = 8;
            // 
            // campoPassword
            // 
            this.campoPassword.Location = new System.Drawing.Point(133, 225);
            this.campoPassword.Name = "campoPassword";
            this.campoPassword.Size = new System.Drawing.Size(217, 20);
            this.campoPassword.TabIndex = 9;
            // 
            // campoPassword2
            // 
            this.campoPassword2.Location = new System.Drawing.Point(133, 258);
            this.campoPassword2.Name = "campoPassword2";
            this.campoPassword2.Size = new System.Drawing.Size(217, 20);
            this.campoPassword2.TabIndex = 10;
            // 
            // campoDataNascimento
            // 
            this.campoDataNascimento.Location = new System.Drawing.Point(133, 154);
            this.campoDataNascimento.Name = "campoDataNascimento";
            this.campoDataNascimento.Size = new System.Drawing.Size(217, 20);
            this.campoDataNascimento.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 157);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Data de Nascimento:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 299);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Esta conta deverá:";
            // 
            // radioDonoAgenda
            // 
            this.radioDonoAgenda.AutoSize = true;
            this.radioDonoAgenda.Location = new System.Drawing.Point(54, 333);
            this.radioDonoAgenda.Name = "radioDonoAgenda";
            this.radioDonoAgenda.Size = new System.Drawing.Size(145, 17);
            this.radioDonoAgenda.TabIndex = 14;
            this.radioDonoAgenda.TabStop = true;
            this.radioDonoAgenda.Text = "Possuir a própria agenda.";
            this.radioDonoAgenda.UseVisualStyleBackColor = true;
            // 
            // radioTerceiro
            // 
            this.radioTerceiro.AutoSize = true;
            this.radioTerceiro.Location = new System.Drawing.Point(206, 333);
            this.radioTerceiro.Name = "radioTerceiro";
            this.radioTerceiro.Size = new System.Drawing.Size(150, 17);
            this.radioTerceiro.TabIndex = 15;
            this.radioTerceiro.TabStop = true;
            this.radioTerceiro.Text = "Gerenciar outras agendas.";
            this.radioTerceiro.UseVisualStyleBackColor = true;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Location = new System.Drawing.Point(74, 379);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 16;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(217, 379);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 17;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // TelaCriarUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 414);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.radioTerceiro);
            this.Controls.Add(this.radioDonoAgenda);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.campoDataNascimento);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.campoPassword2);
            this.Controls.Add(this.campoPassword);
            this.Controls.Add(this.campoUsername);
            this.Controls.Add(this.campoSobrenome);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.campoNome);
            this.Controls.Add(this.label1);
            this.Name = "TelaCriarUsuario";
            this.Text = "Criar nova conta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox campoNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox campoSobrenome;
        private System.Windows.Forms.TextBox campoUsername;
        private System.Windows.Forms.TextBox campoPassword;
        private System.Windows.Forms.TextBox campoPassword2;
        private System.Windows.Forms.TextBox campoDataNascimento;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton radioDonoAgenda;
        private System.Windows.Forms.RadioButton radioTerceiro;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnCancelar;
    }
}