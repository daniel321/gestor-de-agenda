﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gestor_de_Agenda.Controllers;
using Gestor_de_Agenda.Models;

namespace Gestor_de_Agenda.Screens
{
    public partial class TelaLogin : Form
    {
        public TelaLogin()
        {
            InitializeComponent();
        }

        private Usuario usuario;
        private bool logado = false;

        public Usuario getUsuario()
        {
            return usuario;
        }
        public bool getLogado()
        {
            return logado;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btEntrar_Click(object sender, EventArgs e)
        {
            Logger login = new Logger();
            logado = login.logar(campoUsername.Text, campoPassword.Text);
            if (logado)
            {
                usuario = login.getUsuario();
                this.Dispose();
            }
            else
            {
                MessageBox.Show("Usuário ou senha incorretos");
            }
        }

        private void btnCriarConta_Click(object sender, EventArgs e)
        {
            TelaCriarUsuario TelaCriarUsuario = new TelaCriarUsuario();
            TelaCriarUsuario.ShowDialog();
        }        
    }
}
