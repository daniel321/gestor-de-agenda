﻿namespace Gestor_de_Agenda
{
    partial class TelaPrincipal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">verdade se for necessário descartar os recursos gerenciados; caso contrário, falso.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte do Designer - não modifique
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("Hoje", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup5 = new System.Windows.Forms.ListViewGroup("Esta Semana", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup6 = new System.Windows.Forms.ListViewGroup("Este mês", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "Atividade 1",
            "hkjhj",
            "kjhj"}, -1);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("Trabalhar");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("Dormir");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("Estudar");
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGerenciarCompromissos = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnGerenciarTarefas = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtSaudacao = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGerenciarCompromissos);
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Location = new System.Drawing.Point(61, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 437);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Compromissos para hoje.";
            // 
            // btnGerenciarCompromissos
            // 
            this.btnGerenciarCompromissos.Location = new System.Drawing.Point(77, 385);
            this.btnGerenciarCompromissos.Name = "btnGerenciarCompromissos";
            this.btnGerenciarCompromissos.Size = new System.Drawing.Size(132, 23);
            this.btnGerenciarCompromissos.TabIndex = 4;
            this.btnGerenciarCompromissos.Text = "Gerenciar compromissos";
            this.btnGerenciarCompromissos.UseVisualStyleBackColor = true;
            this.btnGerenciarCompromissos.Click += new System.EventHandler(this.btnGerenciarCompromissos_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "Ir ao dentista.",
            "Retornar ligação.",
            "Ir para o casamento."});
            this.listBox1.Location = new System.Drawing.Point(77, 69);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(202, 251);
            this.listBox1.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnGerenciarTarefas);
            this.groupBox2.Controls.Add(this.listView1);
            this.groupBox2.Location = new System.Drawing.Point(552, 59);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(600, 437);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rotina da semana";
            // 
            // btnGerenciarTarefas
            // 
            this.btnGerenciarTarefas.Location = new System.Drawing.Point(54, 385);
            this.btnGerenciarTarefas.Name = "btnGerenciarTarefas";
            this.btnGerenciarTarefas.Size = new System.Drawing.Size(132, 23);
            this.btnGerenciarTarefas.TabIndex = 5;
            this.btnGerenciarTarefas.Text = "Gerenciar tarefas";
            this.btnGerenciarTarefas.UseVisualStyleBackColor = true;
            this.btnGerenciarTarefas.Click += new System.EventHandler(this.btnGerenciarTarefas_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader1});
            listViewGroup4.Header = "Hoje";
            listViewGroup4.Name = "listViewGroup1";
            listViewGroup5.Header = "Esta Semana";
            listViewGroup5.Name = "listViewGroup2";
            listViewGroup6.Header = "Este mês";
            listViewGroup6.Name = "listViewGroup3";
            this.listView1.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup4,
            listViewGroup5,
            listViewGroup6});
            listViewItem5.Group = listViewGroup4;
            listViewItem6.Group = listViewGroup5;
            listViewItem7.Group = listViewGroup5;
            listViewItem8.Group = listViewGroup6;
            this.listView1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8});
            this.listView1.Location = new System.Drawing.Point(54, 69);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(508, 286);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // columnHeader2
            // 
            this.columnHeader2.DisplayIndex = 1;
            // 
            // columnHeader1
            // 
            this.columnHeader1.DisplayIndex = 0;
            this.columnHeader1.Text = "ColumnHeaderfgh";
            // 
            // txtSaudacao
            // 
            this.txtSaudacao.AutoSize = true;
            this.txtSaudacao.Location = new System.Drawing.Point(62, 22);
            this.txtSaudacao.Name = "txtSaudacao";
            this.txtSaudacao.Size = new System.Drawing.Size(83, 13);
            this.txtSaudacao.TabIndex = 2;
            this.txtSaudacao.Text = "Bom dia Fulano.";
            // 
            // TelaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1163, 578);
            this.Controls.Add(this.txtSaudacao);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "TelaPrincipal";
            this.Text = "Gestor de Agenda - Início";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btnGerenciarCompromissos;
        private System.Windows.Forms.Button btnGerenciarTarefas;
        private System.Windows.Forms.Label txtSaudacao;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
    }
}

