﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gestor_de_Agenda.Models;
using Gestor_de_Agenda.Screens;

namespace Gestor_de_Agenda
{
    public partial class TelaPrincipal : Form
    {
        public TelaPrincipal()
        {
            InitializeComponent();
        }
        private Agenda agenda;
        private Usuario usuario;

        public void setUsuario(Usuario usuario)
        {
            this.usuario = usuario;
            txtSaudacao.Text = "Bom dia " + usuario.nome + ".";
            agenda = usuario.solicitaAgenda(usuario);
        }

        private void btnGerenciarCompromissos_Click(object sender, EventArgs e)
        {
            TelaGerenciarCompromissos TelaCompromissos = new TelaGerenciarCompromissos();
            TelaCompromissos.ShowDialog();

        }

        private void btnGerenciarTarefas_Click(object sender, EventArgs e)
        {
            TelaGerenciarRotina TelaRotina = new TelaGerenciarRotina();
            TelaRotina.ShowDialog();
        }
    }
}
